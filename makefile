up: ## Runs admin panel as daemon
	docker-compose up -d
fdown: ## Use it carefully because it removes db volume
	docker-compose down -v